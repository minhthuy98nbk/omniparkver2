package com.example.omniparkver2;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapMarkerActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double x;
    private double y;
    private String title;
    private int size;

    public MapMarkerActivity (Coordinates coordinates, String title, int size, GoogleMap mMap) {
        this.x = coordinates.getX();
        this.y = coordinates.getY();
        this.title = title;
        this.size = size;
        this.mMap = mMap;
        onMapReady(mMap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng Thuy = new LatLng(x, y);
        mMap.addMarker(new MarkerOptions().position(Thuy).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Thuy, size));
    }
}
