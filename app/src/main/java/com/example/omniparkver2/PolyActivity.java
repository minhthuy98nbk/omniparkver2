package com.example.omniparkver2;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import java.util.List;

public class PolyActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnPolylineClickListener {

    private GoogleMap mMap;
    private List<Coordinates> coordinatesList;
    private Polyline polyline;
    private static final int COLOR_GREEN_ARGB = 0xff00cc00;
    private static final int POLYLINE_STROKE_WIDTH_PX = 12;

//    private static final int PATTERN_GAP_LENGTH_PX = 20;
//    private static final PatternItem DOT = new Dot();
//    private static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);
//    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);

    public PolyActivity (List<Coordinates> coordinatesList, GoogleMap mMap) {
        this.mMap = mMap;
        this.coordinatesList = coordinatesList;
        onMapReady(mMap);
        onPolylineClick(polyline);
    }
    @Override
    public void onMapReady(GoogleMap mMap) {
        final PolylineOptions polylineOptions = new PolylineOptions().clickable(true);
        for (Coordinates t : coordinatesList) {
            polylineOptions.add(new LatLng(t.getX(),t.getY()));
        }
        polyline = mMap.addPolyline(polylineOptions);
        stylePolyline();
        mMap.setOnPolylineClickListener(this);
    }

    private void stylePolyline() {
        polyline.setStartCap(new RoundCap());
        polyline.setEndCap(new RoundCap());
        polyline.setWidth(POLYLINE_STROKE_WIDTH_PX);
        polyline.setColor(COLOR_GREEN_ARGB);
        polyline.setJointType(JointType.ROUND);
    }


    @Override
    public void onPolylineClick(Polyline polyline) {
//        if ((polyline.getPattern() == null) || (!polyline.getPattern().contains(DOT))) {
//            polyline.setPattern(PATTERN_POLYLINE_DOTTED);
//        } else {
//            // The default pattern is a solid stroke.
//            polyline.setPattern(null);
//        }
//        polyline.setPattern(null);
//        Toast.makeText(this, "Route type " + polyline.getTag().toString(), Toast.LENGTH_SHORT).show();
    }
}