package com.example.omniparkver2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class DriverLoginAcitivity extends AppCompatActivity {

    private static final String TAG = "DriverLoginAcitivity";

    private static EditText edtUserField;
    private static EditText edtPasswordField;

    private static Button btnTitle;
    private static Button btnLogin;
    private static TextView tvSignup;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        init();

    }



    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    private void init(){
        btnTitle = (Button) findViewById(R.id.btnTitle);
        btnTitle.setText("DRIVER");
        edtUserField = (EditText) findViewById(R.id.edtUser);
        edtPasswordField = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvSignup = (TextView) findViewById(R.id.tvSignup);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLogIn();
//                Intent intent = new Intent(DriverLoginAcitivity.this, MapActivity.class);
//                startActivity(intent);
            }
        });
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverLoginAcitivity.this, DriverSignupAcitivity.class);
                startActivity(intent);
            }
        });

    }

    private boolean checkEmpty(String... edts){
        for (String edt:edts){
            if (edt.equals("")) {
                return false;
            }
        }
        return true;
    }

    private void startLogIn(){
        String email = edtUserField.getText().toString();
        String password = edtPasswordField.getText().toString();

        if (checkEmpty(email,password)==false){
            Toast.makeText(DriverLoginAcitivity.this, "Hãy điền đầy đủ các trường",
                    Toast.LENGTH_LONG).show();
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(DriverLoginAcitivity.this, MapActivity.class);
                            startActivity(intent);


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(DriverLoginAcitivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

}






















