package com.example.omniparkver2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;


public class ContributorManagerAcitivity extends AppCompatActivity {

    private static final String TAG = "ContributorLoginAcitivity";

    private static Button btnTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_manager);
        init();

    }
    private void init(){
        List<Item> image_details = getListData();
        final GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new CustomGridContributorManagerAdapter(this, image_details));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Object o = gridView.getItemAtPosition(position);
                Item item = (Item) o;
                Toast.makeText(ContributorManagerAcitivity.this, "Selected :"
                        + " " + item, Toast.LENGTH_LONG).show();
                if (((Item) o).getLabel().equals("Parking Status")) {
                    Intent intent = new Intent(ContributorManagerAcitivity.this, ParkingStatusAcitivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private  List<Item> getListData() {
        List<Item> list = new ArrayList<>();
        list.add(new Item("status","Parking Status"));
        list.add(new Item("comfirm","Comfirm New Car"));
        list.add(new Item("query","Query"));
        list.add(new Item("callcenter","Call Center"));
        return list;
    }

}






















