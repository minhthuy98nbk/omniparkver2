package com.example.omniparkver2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;


public class DriverSignupAcitivity extends AppCompatActivity {

    private static final String TAG = "DriverSignupAcitivity";

    private static Button btnTitle;
    private static Button btnSignup;

    private static EditText edtPhone;
    private static EditText edtEmail;
    private static EditText edtLicensePlates;
    private static EditText edtPassword;
    private static EditText edtComfirmPassword;

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_signup);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        init();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    private boolean checkEmpty(String... edts){
        for (String edt:edts){
            if (edt.equals("")) {
                return false;
            }
        }
        return true;
    }

    private void init(){

        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtLicensePlates = (EditText) findViewById(R.id.edtLicensePlate);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtComfirmPassword = (EditText) findViewById(R.id.edtComfirmPassword);

        btnSignup = (Button) findViewById(R.id.btnSignUp);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String phone = edtPhone.getText().toString();
                final String email = edtEmail.getText().toString();
                final String licensePlates = edtLicensePlates.getText().toString();
                final String password = edtPassword.getText().toString();
                final String confirmPassword = edtComfirmPassword.getText().toString();

                if (checkEmpty(phone,email,licensePlates,password,confirmPassword) == false) {
                    Toast.makeText(DriverSignupAcitivity.this, "Hãy điền đầy đủ các trường", Toast.LENGTH_LONG).show();
                    return;
                }
                if (password.equals(confirmPassword)==false){
                    Log.d(TAG, "Mật khẩu không khớp nhau!");
                    Toast.makeText(DriverSignupAcitivity.this, "Mật khẩu không khớp nhau!", Toast.LENGTH_LONG).show();
                    return;
                }
                mAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(DriverSignupAcitivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Driver driver = new Driver(phone,email,licensePlates);
                                database.getReference("User")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(driver).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                      Intent intent = new Intent(DriverSignupAcitivity.this, DriverSignupCodeComfirmAcitivity.class);
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(DriverSignupAcitivity.this, "Authentication failed.",Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
        });
    }
}






















