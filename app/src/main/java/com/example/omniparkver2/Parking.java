package com.example.omniparkver2;

public class Parking {
    private String name;
    private String address;
    private int capacity;
    private int emptySlot;
    private int numberOfComment;
    private double mark;
    private double distantToTarget;
    private double distantFromHere;

    public Parking(String name, String address, int capacity, int emptySlot, int numberOfComment, double mark, double distantToTarget, double distantFromHere){
        this.name = name;
        this.address = address;
        this.capacity= capacity;
        this.emptySlot = emptySlot;
        this.numberOfComment = numberOfComment;
        this.mark = mark;
        this. distantFromHere = distantFromHere;
        this.distantToTarget = distantToTarget;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getDistantFromHere() {
        return distantFromHere;
    }

    public void setDistantFromHere(double distantFromHere) {
        this.distantFromHere = distantFromHere;
    }

    public double getDistantToTarget() {
        return distantToTarget;
    }

    public void setDistantToTarget(double distantToTarget) {
        this.distantToTarget = distantToTarget;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setEmptySlot(int emptySlot) {
        this.emptySlot = emptySlot;
    }

    public int getEmptySlot() {
        return emptySlot;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public double getMark() {
        return mark;
    }

    public void setNumberOfComment(int numberOfComment) {
        this.numberOfComment = numberOfComment;
    }

    public int getNumberOfComment() {
        return numberOfComment;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
