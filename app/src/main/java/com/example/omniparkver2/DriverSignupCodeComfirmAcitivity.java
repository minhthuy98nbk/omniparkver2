package com.example.omniparkver2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class DriverSignupCodeComfirmAcitivity extends AppCompatActivity {

    private static final String TAG = "DriverSignupCodeComfirmAcitivity";

    private static Button btnTitle;
    private static Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_signup_codecomfirm);
        init();

    }
    private void init(){

        btnSignup = (Button) findViewById(R.id.btnOk);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverSignupCodeComfirmAcitivity.this, DriverLoginAcitivity.class);
                startActivity(intent);
            }
        });
    }

}






















