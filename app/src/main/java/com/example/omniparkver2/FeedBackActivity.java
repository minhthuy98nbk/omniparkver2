package com.example.omniparkver2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class FeedBackActivity extends AppCompatActivity {

    private Button btnCanPark;
    private Button btnCannotPark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        init();
    }

    private void init(){
        btnCanPark = (Button) findViewById(R.id.btnCanPark);
        btnCannotPark = (Button) findViewById(R.id.btnCannotPark);

        btnCannotPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FeedBackActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        btnCanPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

    }

    private void showDialog() {

//        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
//                .session(3)
//                .threshold(3)
//                .ratingBarColor(R.color.yellow)
//                .playstoreUrl("https://github.com/codemybrainsout/smart-app-rate")
//                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
//                    @Override
//                    public void onFormSubmitted(String feedback) {
//                        Log.i(TAG,"Feedback:" + feedback);
//                    }
//                })
//                .build();
//
//
//        ratingDialog.show();
    }

}
