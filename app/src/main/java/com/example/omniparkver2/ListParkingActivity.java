package com.example.omniparkver2;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListParkingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_parking);

        List<Parking> image_details = getListData();
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new CustomListParkingAdapter(this, image_details));

        // Khi người dùng click vào các ListItem
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//                Object o = listView.getItemAtPosition(position);
//                Parking country = (Parking) o;
//                Toast.makeText(ListParkingActivity.this, "Selected :" + " " + country, Toast.LENGTH_LONG).show();
//            }
//        });

    }


    private List<Parking> getListData() {
        List<Parking> list = new ArrayList<>();

        Parking p1 = new Parking("Bãi 1", "Quận Hải Châu", 100, 56, 374, 9.0, 0.5, 3.6);
        list.add(p1);

        Parking p2 = new Parking("Bãi 2", "Quận Sơn Trà", 70, 34, 123, 9.3, 0.8, 2.7);
        list.add(p2);

        Parking p3 = new Parking("Bãi 3", "Quận Thanh Khê", 65, 17, 98, 8.6, 1.2, 4.5);
        list.add(p3);

        Parking p4 = new Parking("Bãi 1", "Quận Hải Châu", 100, 56, 374, 9.0, 0.5, 3.6);
        list.add(p4);

        Parking p5 = new Parking("Bãi 2", "Quận Sơn Trà", 70, 34, 123, 9.3, 0.8, 2.7);
        list.add(p5);

        Parking p6 = new Parking("Bãi 3", "Quận Thanh Khê", 65, 17, 98, 8.6, 1.2, 4.5);
        list.add(p6);

        return list;
    }

}
