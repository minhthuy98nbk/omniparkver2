package com.example.omniparkver2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomListParkingAdapter  extends BaseAdapter {
    private List<Parking> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListParkingAdapter(Context aContext,  List<Parking> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_parking_infor_layout, null);
            holder = new ViewHolder();
            holder.nameView = (TextView) convertView.findViewById(R.id.tvName);
            holder.addressView = (TextView) convertView.findViewById(R.id.tvAddress);
            holder.numberOfCommentView = (TextView) convertView.findViewById(R.id.tvNumberOfComment);
            holder.markView = (TextView) convertView.findViewById(R.id.tvMark);
            holder.capacityView = (TextView) convertView.findViewById(R.id.tvCapacity);
            holder.distantToTargetView = (TextView) convertView.findViewById(R.id.tvDistantToTarget);
            holder.distantFromHereView = (TextView) convertView.findViewById(R.id.tvDistantFromHere);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Parking parking = this.listData.get(position);
        holder.nameView.setText(parking.getName());
        holder.addressView.setText(parking.getAddress());
        holder.numberOfCommentView.setText(parking.getNumberOfComment()+ "");
        holder.markView.setText(parking.getMark() + "");
        holder.capacityView.setText("Sức chứa: " + parking.getEmptySlot() + " chỗ trống/ " + parking.getCapacity() + " chỗ đỗ xe");
        holder.distantFromHereView.setText(parking.getDistantFromHere() + "km từ đây");
        holder.distantToTargetView.setText(parking.getDistantToTarget() + "km đến đích");
        return convertView;
    }


    static class ViewHolder {
        TextView nameView;
        TextView addressView;
        TextView numberOfCommentView;
        TextView markView;
        TextView capacityView;
        TextView distantToTargetView;
        TextView distantFromHereView;
    }

}
